import mocker
from com.appspot.ecfutebolarte.handler.PrincipalHandler import PrincipalHandler

class PrincipalHandlerTestCase(mocker.MockerTestCase):
    def setUp(self):
        self.request = self.mocker.mock()
        self.response = self.mocker.mock()
        self.handler = PrincipalHandler()
        self.handler.request = self.request
        self.handler.response = self.response
        self.users = self.mocker.replace('google.appengine.api.users')
        self.template = self.mocker.replace('google.appengine.ext.webapp.template')

    def testGetWhenUserNotLogedOnAndActionIsDifferentOfLogon(self):
        self.request.get('action')
        self.mocker.result('')
        
        fake_user = self.mocker.mock()
        self.users.get_current_user()
        self.mocker.result(None)
        
        out = self.mocker.mock() 
        self.response.out
        self.mocker.result(out)
        
        self.template.render(mocker.ANY, mocker.ANY)
        def checkArgs(path, params):
            values = {}
            self.assert_(path.endswith('view/index-out.html'))
            self.assertEqual(values, params)
            return '<html />'
        self.mocker.call(checkArgs)
        out.write('<html/>')
        self.mocker.replay()
        self.handler.get()

#########################
# LAUNCH ALL UNIT TESTS #
#########################
if __name__ == '__main__':
    unittest.main()
