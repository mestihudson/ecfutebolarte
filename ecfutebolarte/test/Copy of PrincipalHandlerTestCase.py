import mocker
from com.appspot.ecfutebolarte.handler.PrincipalHandler import PrincipalHandler

class PrincipalHandlerTestCase(mocker.MockerTestCase):
    def setUp(self):
        self.request = self.mocker.mock()
        self.response = self.mocker.mock()
        self.handler = PrincipalHandler()
        self.handler.request = self.request
        self.handler.response = self.response
        self.users = self.mocker.replace('google.appengine.api.users')
        self.template = self.mocker.replace('google.appengine.ext.webapp.template')

    def testGetWhenUserNotLogedOnAndActionIsDifferentOfLogon(self):
        ''' 
            dizemos para quando do metodo request.get('action')
            for chamada, retornar uma string vazia
        '''
        self.request.get('action')
        self.mocker.result('')
        ''' 
            criamos um usuario fake
            e fazermos o método get_current_user() do objeto
            implicito users retornar None
        '''
        fake_user = self.mocker.mock()
        self.users.get_current_user()
        self.mocker.result(None)
        '''
            criamos uma saída mock e dizemos que
            ao ser referenciada a propriedade out do
            objeto response, retornará esse mock
        '''
        out = self.mocker.mock() 
        self.response.out
        self.mocker.result(out)
        '''
            dizemos que ao ser chamado o método template.render, 
            passando dois argumentos quaisquer
        '''
        self.template.render(mocker.ANY, mocker.ANY)
        def checkArgs(path, params):
            values = {}
            self.assert_(path.endswith('view/index-out.html'))
            self.assertEqual(values, params)
            return '<html />'
        '''
            finalmente finalizamos a configuração do mock
            e executamos o método a ser testado
        '''
        self.mocker.replay()
        self.handler.get()
        
        # dizemos que a propriedade uri do request retorna um valor
#        self.request.uri
#        self.mocker.result('fake uri')
#        self.users.create_login_url('fake uri')
#        self.mocker.result('fake logon uri')
#        mock_redirect = self.mocker.mock()
#        self.handler.redirect = lambda x: mock_redirect.redirect(x)
#        mock_redirect.redirect('fake logon uri')
        
#########################
# LAUNCH ALL UNIT TESTS #
#########################
if __name__ == '__main__':
    unittest.main()
