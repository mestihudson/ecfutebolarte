'''
Created on 02/04/2009

@author: mestihudson@gmail.com
'''
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from com.appspot.ecfutebolarte.handler.PrincipalHandler import PrincipalHandler 
from com.appspot.ecfutebolarte.handler.ManterPeladaHandler import ManterPeladaHandler
from com.appspot.ecfutebolarte.handler.ManterPeladeiroHandler import  ManterPeladeiroHandler
from com.appspot.ecfutebolarte.handler.ManterMovimentacaoFinanceiraHandler import  ManterMovimentacaoFinanceiraHandler
from com.appspot.ecfutebolarte.handler.ManterArtilhariaHandler import  ManterArtilhariaHandler
from com.appspot.ecfutebolarte.handler.ManterPeladeiroAcessoHandler import  ManterPeladeiroAcessoHandler
from com.appspot.ecfutebolarte.handler.ManterModuloHandler import  ManterModuloHandler 

def main():
    application = webapp.WSGIApplication([
                                          ('/', PrincipalHandler),
                                          ('/pelada', ManterPeladaHandler),
                                          ('/peladeiro', ManterPeladeiroHandler), 
                                          ('/financeiro', ManterMovimentacaoFinanceiraHandler),
                                          ('/artilharia', ManterArtilhariaHandler),
                                          ('/acesso', ManterPeladeiroAcessoHandler),
                                          ('/modulo', ManterModuloHandler)
                                          ], debug=True)
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
