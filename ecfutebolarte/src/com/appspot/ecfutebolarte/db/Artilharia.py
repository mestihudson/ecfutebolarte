from google.appengine.ext import db
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.Pelada import Pelada
from com.appspot.ecfutebolarte.db.Presenca import Presenca

class Artilharia(db.Model):
    peladeiro = db.ReferenceProperty(Peladeiro)
    pelada = db.ReferenceProperty(Pelada)
    presenca = db.ReferenceProperty(Presenca)
    gols = db.IntegerProperty()    
