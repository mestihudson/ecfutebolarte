from google.appengine.ext import db

class Pelada(db.Model):
    local = db.StringProperty()
    dataHora = db.DateTimeProperty()
    quorumMinimo = db.IntegerProperty()
    quorumMaximo = db.IntegerProperty()
    valorPorPessoa = db.FloatProperty()
    observacao = db.StringProperty()
