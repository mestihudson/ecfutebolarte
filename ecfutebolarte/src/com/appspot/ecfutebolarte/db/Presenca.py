'''
Created on 06/04/2009

@author: 84571713304
'''
from google.appengine.ext import db
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.Pelada import Pelada

class Presenca(db.Model):
    peladeiro = db.ReferenceProperty(Peladeiro)
    pelada = db.ReferenceProperty(Pelada)
    confirmada = db.BooleanProperty()
