'''
Created on 06/04/2009

@author: 84571713304
'''
from google.appengine.ext import db
from com.appspot.ecfutebolarte.db.Presenca import Presenca

class Justificativa(db.Model):
    presenca = db.ReferenceProperty(Presenca)
    observacao = db.StringProperty()
