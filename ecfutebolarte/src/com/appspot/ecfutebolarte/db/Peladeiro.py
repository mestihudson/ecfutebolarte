from google.appengine.ext import db

class Peladeiro(db.Model):
    user = db.UserProperty()
    nome = db.StringProperty()
    apelido = db.StringProperty()
    telefone = db.StringProperty()
    confirmacao = db.DateTimeProperty()
    gols = db.StringProperty()
    numeroPartidas = db.IntegerProperty()
    perfil = db.IntegerProperty()
