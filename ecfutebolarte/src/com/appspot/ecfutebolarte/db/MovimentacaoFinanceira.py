from google.appengine.ext import db
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.MotivoMovFinanceira import MotivoMovFinanceira

class MovimentacaoFinanceira(db.Model):
    peladeiro = db.ReferenceProperty(Peladeiro)
    data = db.DateTimeProperty()
    valor = db.FloatProperty()
    tipo = db.IntegerProperty()
    motivo = db.ReferenceProperty(MotivoMovFinanceira)
    valorFormatado = db.StringProperty() 
    comentario =  db.StringProperty()
    
