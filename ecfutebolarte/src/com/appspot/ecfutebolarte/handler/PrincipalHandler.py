from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template 
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro

class PrincipalHandler(webapp.RequestHandler):

    def post(self):
        self.process()

    def get(self):
        self.process()

    def process(self):
        action = self.request.get('action')
        if not users.get_current_user():
            if action == 'logon':
                url = users.create_login_url(self.request.uri)
                self.redirect(url)
            else:
                out = template.render('view/index-out.html', {})
                self.response.out.write(out)
        else:
            if action == 'logoff':
                url = users.create_logout_url(self.request.uri)
                self.redirect(url)
            else:
                peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
                if not peladeiro:
                    if users.is_current_user_admin():
                        peladeiro = Peladeiro(user=users.get_current_user())
                        key = peladeiro.put()
                        peladeiro = Peladeiro.get(key)
                    else:
                        out = template.render('view/fatal-error.html', {})
                        self.response.out.write(out)
                        return
                if not peladeiro.confirmacao:
                    self.redirect('/peladeiro?action=confirm')
                else:
                    values = {                    
                    'peladeiro':peladeiro
                    }
                    out = template.render('view/index-in.html', values)
                    self.response.out.write(out)
