from google.appengine.api import mail
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import login_required
from datetime import datetime
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.PeladeiroAcesso import PeladeiroAcesso
from com.appspot.ecfutebolarte.db.Modulo import Modulo


class ManterPeladeiroAcessoHandler(webapp.RequestHandler):

    def post(self):
        self.process()

    def get(self):
        self.process()
        
    def process(self):
        self.response.headers['Content-Type'] = 'text/html; charset=iso-8859-1'
        if self.auth() and self.acesso():
            action = self.request.get('action')
            if action == '':
                self.list()
            if action == 'list':
                self.list()
            if action == 'edit':
                self.edit(self.request.get('id'), {})
            if action == 'save':
                self.save()
  
    def list(self):
        peladeiros = Peladeiro.all()
        modulos = Modulo.all()
        peladeirosAcessos = PeladeiroAcesso.all()
        values = {
            'peladeiros': peladeiros,
            'modulos': modulos,
            'peladeirosAcessos': peladeirosAcessos
        }
        self.response.out.write(template.render('view/acesso/index.html', values))

    def edit(self, key, message):
        modulos = Modulo.all()
        peladeiro = Peladeiro.get(key)
        peladeiro.id = key
        
        peladeirosAcessos = PeladeiroAcesso.all().filter('peladeiro = ', peladeiro)
        
        values = {
            'message': message,
            'peladeiro': peladeiro,
            'modulos': modulos,
            'peladeirosAcessos': peladeirosAcessos
        }
        self.response.out.write(template.render('view/acesso/form.html', values))

    def auth(self):
        if not users.get_current_user():
            if self.request.method != 'GET':
                self.redirect('/?action=logon')
            else:
                url = users.create_login_url(self.request.uri)
                self.redirect(url)
        else:
            peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
            if not peladeiro.confirmacao:
                self.redirect('/')
            else:
                return True

    def save(self):
        peladeiro = Peladeiro.get(self.request.get('id'))
        
        auxPeladeiro = PeladeiroAcesso.all().get()
        acessos = PeladeiroAcesso.all().filter('peladeiro = ', peladeiro)
        if acessos.count()>0:
            db.delete(acessos)

        for modulo in Modulo.all():
            if self.request.get(modulo.nome) != '':
                acesso = PeladeiroAcesso()
                acesso.peladeiro = peladeiro
                acesso.modulo = modulo
                acesso.put()
        
        self.list()        
        
    def acesso(self):
        if users.is_current_user_admin(): 
            return True
        else:
            self.response.out.write(template.render('view/semacesso.html',''))
