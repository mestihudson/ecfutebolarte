from google.appengine.api import mail
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import login_required
from datetime import datetime
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro


class ManterPeladeiroHandler(webapp.RequestHandler):

    def post(self):
        self.process()

    def get(self):
        self.process()
        
    def process(self):
        if self.auth():
            action = self.request.get('action')
            if action == '':
                action = 'list'
            if action == 'new':
                if not users.is_current_user_admin():
                    self.response.out.write(template.render('view/exclusivoadministrador.html',''))
                else:
                    self.new()
            if action == 'list':
                self.list()
            if action == 'editNew':
                self.editNew(self.request.get('id'), {})
            if action == 'edit':
                self.edit(self.request.get('id'), {})
            if action == 'delete':
                self.delete()
            if action == 'save':
                self.save()
            if action == 'saveNew':
                self.saveNew()
            if action == 'warn':
                self.warn(self.request.get('id'), True)
            if action == 'confirm':
                self.confirm()   
            if action == 'view':
                self.view(self.request.get('id'), {})     
  
    def auth(self):
        if not users.get_current_user():
            if self.request.method != 'GET':
                self.redirect('/?action=logon')
            else:
                url = users.create_login_url(self.request.uri)
                self.redirect(url)
        else:
            peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
            if not peladeiro.confirmacao:
                self.confirm()
                return False
            else:
                return True

    def saveNew(self):
       if self.request.get('id') == '':    #new
            isNew = True
            user = self.request.get('user') + '@gmail.com' 
            peladeiro = Peladeiro.all().filter('user = ', users.User(user)).get()
            if not peladeiro:
                peladeiro = Peladeiro()
                peladeiro.apelido = self.request.get('apelido')
                peladeiro.telefone = self.request.get('telefone');               
            else:
                message = {
                    'error':'Usu&aacute;rio "' + self.request.get('user') + '" j&aacute; cadastrado.'
                }
       else:     
           isNew = False
           peladeiro = Peladeiro.get(self.request.get('id'))
           
       peladeiro.user = users.User(self.request.get('user') + '@gmail.com')
       peladeiro.nome = self.request.get('nome')
       key = peladeiro.put()
       if isNew:
           message = {
               'info':'Usu&aacute;rio "' + self.request.get('user') + '" cadastrado com sucesso.'
           }
       else:
           message = {
               'info':' Dados do usu&aacute;rio "' + self.request.get('user') + '" salvos com sucesso.'
           }
       self.editNew(key, message)                       
        
    def save(self):
        if self.request.get('id') == '':    #new
            isNew = True
            user = self.request.get('user') + '@gmail.com'
            peladeiro = Peladeiro.all().filter('user = ', users.User(user)).get()
            if not peladeiro:
                peladeiro = Peladeiro()
            else:
                message = {
                    'error':'Usu&aacute;rio "' + self.request.get('user') + '" j&aacute; cadastrado.'
                }
                self.edit(peladeiro.key(), message)
                return
        else:                               #edit
            isNew = False
            peladeiro = Peladeiro.get(self.request.get('id'))
        peladeiro.user = users.User(self.request.get('user') + '@gmail.com')
        peladeiro.nome = self.request.get('nome')
        peladeiro.apelido = self.request.get('apelido')
        peladeiro.telefone = self.request.get('telefone')
        key = peladeiro.put()
        if isNew:
            message = {
                'info':'Usu&aacute;rio "' + self.request.get('user') + '" cadastrado com sucesso.'
            }
        else:
            message = {
                'info':' Dados do usu&aacute;rio "' + self.request.get('user') + '" salvos com sucesso.'
            }
        self.redirect('/peladeiro') 

    def new(self):
        peladeiro = Peladeiro()
        values = {
            'peladeiro': peladeiro
        }
        self.response.out.write(template.render('view/peladeiro/new.html', values))

    def edit(self, key, message):
        peladeiro = Peladeiro.get(key)
        peladeiro.id = key
        values = {
            'message': message,
            'peladeiro': peladeiro
        }
        self.response.out.write(template.render('view/peladeiro/form.html', values))
    
    def view(self, key, message):
        peladeiro = Peladeiro.get(key)
        peladeiro.id = key
        values = {
            'message': message,
            'peladeiro': peladeiro
        }
        self.response.out.write(template.render('view/peladeiro/view.html', values))

    def editNew(self, key, message):
        peladeiro = Peladeiro.get(key)
        peladeiro.id = key
        values = {
            'message': message,
            'peladeiro': peladeiro
        }
        peladeiro.put()
        self.redirect('/peladeiro')        
        
    def delete(self):
        if users.is_current_user_admin(): 
            key = self.request.get('id')
            peladeiro = Peladeiro.get(key)
            db.delete(peladeiro)
            self.list()
            #return True
        else:
            self.response.out.write(template.render('view/exclusivoadministrador.html',''))
        

    def list(self):
        peladeiros = Peladeiro.gql("ORDER BY user ASC")  
        usuario = Peladeiro.all().filter('user = ', users.get_current_user()).get() 
        administrador = users.is_current_user_admin()             
        values = {
            'peladeiros': peladeiros,
            'usuario': usuario,
            'administrador':administrador
        }
        self.response.out.write(template.render('view/peladeiro/index.html', values))
    
    def warn(self, key):
        peladeiro = Peladeiro.get(key)
        message = mail.EmailMessage()
        message.sender = 'ecfutebolarte@gmail.com'
        message.to = peladeiro.user + '@gmail'
        message.subject = 'Confirmacao de Cadastro no Esporte Clube Futebol Arte'
        message.body = '''
        Caro %s,
        
        Um dos administradores do  Esporte Clube Futebol Arte (ecfutebolarte.appspot.com), lhe enviou 
        essa menssagem para que voce confirme seu cadastro no sitio.
        
        http://ecfutebolarte.appspot.com/peladeiro?action=confirm
        
        Atenciosamente,
        
        Esporte Clube Futebol Arte.
        ''' % (peladeiro.user)
        message.send()

    def confirm(self):
        peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
        if not peladeiro.confirmacao:
            if self.request.get('do') == 'save':                
                peladeiro.nome = self.request.get('nome')
                peladeiro.apelido = self.request.get('apelido')
                peladeiro.telefone = self.request.get('telefone')
                peladeiro.confirmacao = datetime.today() 
                peladeiro.put()
                self.redirect('/')
            else:
                peladeiro.nome = ''
                peladeiro.apelido = ''
                peladeiro.telefone = ''
                values = {
                    'do':'save',
                    'peladeiro':peladeiro
                }
                self.response.out.write(template.render('view/peladeiro/confirm.html', values))
        else:
            self.redirect('/')
