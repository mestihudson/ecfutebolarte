from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import login_required  
from google.appengine.api import mail
from datetime import datetime
from email.Header import Header
from email.MIMEText import MIMEText
from com.appspot.ecfutebolarte.db.Pelada import Pelada
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.Presenca import Presenca

class ManterPeladaHandler(webapp.RequestHandler):

    def post(self):
        self.process()

    def get(self):
        self.process()

    def auth(self):
        if not users.get_current_user():
            if self.request.method != 'GET':
                self.redirect('/?action=logon')
            else:
                url = users.create_login_url(self.request.uri)
                self.redirect(url)
        else:
            peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
            if not peladeiro.confirmacao:
                self.redirect('/')
            else:
                return True

    def process(self):
        if self.auth():
            action = self.request.get('action')
            if action == '':
                action = 'list'
            if action == 'new':
                if not users.is_current_user_admin():
                    self.response.out.write(template.render('view/exclusivoadministrador.html',''))
                else:
                    self.new()
            if action == 'list':
                self.list()
            if action == 'edit':
                if not users.is_current_user_admin():
                    self.response.out.write(template.render('view/exclusivoadministrador.html',''))
                else:
                    self.edit(self.request.get('id'))
            if action == 'show':
                self.show(self.request.get('id'))
            if action == 'delete':
                if not users.is_current_user_admin():
                    self.response.out.write(template.render('view/exclusivoadministrador.html',''))
                else:
                    self.delete()
            if action == 'save':
                self.save()
            if action == 'confirm':
                self.confirm()
            if action == 'furou':
                self.furou()
            if action == 'warn':
                self.warn(self.request.get('id'), True)    
        
    def save(self):
        if self.request.get('id') == '':
            pelada = Pelada()
        else:
            pelada = Pelada.get(self.request.get('id'))
        pelada.local = self.request.get('local')
        try:
            mydatahora = datetime.strptime(self.request.get('data') + ' ' + self.request.get('horario'), '%d/%m/%Y %H:%M:%S')
        except:
            mydatahora = datetime.today()
        pelada.dataHora = mydatahora
        pelada.quorumMinimo = int(self.request.get('quorumMinimo'))
        pelada.quorumMaximo = int(self.request.get('quorumMaximo'))
        pelada.valorPorPessoa = float(self.request.get('valorPorPessoa'))
        pelada.observacao = self.request.get('observacao')
        key = pelada.put()
        self.warn(key, False)
        self.edit(key)
        self.redirect('/pelada')

    def new(self):
        pelada = Pelada(local='', dataHora=datetime.today(), quorumMinimo=10, quorumMaximo=20, valorPorPessoa=7.0, observacao='')
        values = {
            'pelada': pelada
        }
        self.response.out.write(template.render('view/pelada/form.html', values))

    def edit(self, key):
        pelada = Pelada.get(key)
        pelada.id = key
        values = {
            'pelada': pelada
        }
        self.response.out.write(template.render('view/pelada/form.html', values))

    def show(self, key):          
        pelada = Pelada.get(key)
        peladeiros = Peladeiro.all()
        peladas = Pelada.all()
        usuario = Peladeiro.all().filter('user = ', users.get_current_user()).get()
        presentes = Presenca.all()                             
        pelada.id = key          
        #aqui esta sendo testado
        valor = 0
        confirma = False
        for f in presentes:                                
            if str(f.pelada.key()) == pelada.id:                                                 
                if f.confirmada:                                       
                    valor = valor + 1  
        if valor >= 10:
            confirma = True 
            
        values = {
            'pelada': pelada,
            'peladeiros':peladeiros,
            'usuario':usuario,
            'presentes':presentes,
            'valor': valor,
            'confirma':confirma            
        }
        self.response.out.write(template.render('view/pelada/detalhes.html', values))
        
    def delete(self):
        key = self.request.get('id')
        pelada = Pelada.get(key)
        db.delete(pelada)
        self.list()
    
    def furou(self):
        key = self.request.get('id_presenca')        
        presenca = Presenca.get(key)
        presentes = Presenca.all()
        usuario = Peladeiro.all().filter('user = ', users.get_current_user()).get()
        presenca.confirmada = False
        presenca.put()
        self.redirect('/pelada')      
    
    def confirm(self):
        if self.request.get('id_presenca') == '':
            presenca = Presenca()
            force_insert=True
        else:
            presenca = Presenca.get(self.request.get('id_presenca'))
            db.delete(presenca)
            force_update=True   
                 
        presenca.peladeiro = Peladeiro.get(self.request.get('id_peladeiro'))
        presenca.pelada = Pelada.get(self.request.get('id_pelada'))
        presenca.confirmada = True
        presenca.put()
        self.redirect('/pelada?action=show&id=' + self.request.get('id_pelada'))

    def list(self): 
        data = datetime.today()      
        peladas = Pelada.gql("WHERE dataHora > :1 ORDER BY dataHora DESC",data)
        peladasPassada = Pelada.gql("WHERE dataHora < :1 ORDER BY dataHora DESC",data)                
        administrador = users.is_current_user_admin() 
        values = {
            'peladas': peladas,
            'peladasPassada': peladasPassada,
            'administrador': administrador,
            'data':data
        }
        self.response.out.write(template.render('view/pelada/index.html', values))
        
    def warn(self, key, list):
        pelada = Pelada.get(key)
        peladeiros = Peladeiro.all()
        for peladeiro in peladeiros:
            message = mail.EmailMessage()
            message.sender = users.get_current_user().email()
            message.to = peladeiro.user.email()
            message.subject = 'Pelada Marcada' 
            message.body = '''            
            
            Caro %s,
            
            Essa &eacute; uma mensagem do Esporte Clube Futebol Arte informando que uma nova pelada foi marcada.
            Confirme em nosso site: http://ecfutebolarte.appspot.com
            
            Local: %s
            Data: %s
            Hor&aacute;rio: %s
            
            Contamos com sua presen&ccetil;a.
            
            Atenciosamente,
            
            
            Esporte Clube Futebol Arte.
            ''' % (peladeiro.nome, pelada.local, datetime.strftime(pelada.dataHora, '%d/%m/%Y'), datetime.strftime(pelada.dataHora, '%H:%M:%S'))            
            message.send()
        if list:
            self.list()
