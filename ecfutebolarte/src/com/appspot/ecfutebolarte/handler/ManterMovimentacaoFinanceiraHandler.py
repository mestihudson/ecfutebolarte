# -*- coding: latin-1 -*-

from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from datetime import datetime
from django.core import serializers
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.MovimentacaoFinanceira import MovimentacaoFinanceira
from com.appspot.ecfutebolarte.db.MotivoMovFinanceira import MotivoMovFinanceira
from com.appspot.ecfutebolarte.db.PeladeiroAcesso import PeladeiroAcesso
from com.appspot.ecfutebolarte.db.Modulo import Modulo

class ManterMovimentacaoFinanceiraHandler(webapp.RequestHandler):
    def new(self):
        peladeiros = Peladeiro.gql("ORDER BY nome")
#        
#        motivos = MotivoMovFinanceira.all()
#        for m in motivos:
#            MotivoMovFinanceira.delete(m)
#        
#        motivo1 = MotivoMovFinanceira()
#        motivo1.motivo = "Mensalidade"
#        key = motivo1.put()
#        
#        motivo2 = MotivoMovFinanceira()
#        motivo2.motivo = "Partida Avulsa"
#        key = motivo2.put()
#
#        motivo3 = MotivoMovFinanceira()
#        motivo3.motivo = "Outro"
#        key = motivo3.put()

        motivos = MotivoMovFinanceira.all()
        
        values = {
                'peladeiros': peladeiros,
                'motivos': motivos,
                'tipo':'1',
                'mensagem': ''
                }
        
        self.response.out.write(template.render('view/financeiro/form.html',values))

    def list(self, tipo):
        movimentacoesFinanceiras = MovimentacaoFinanceira.gql("ORDER BY data DESC")
        
        total = 0
        for movimentacao in  movimentacoesFinanceiras:
            movimentacao.valorFormatado = 'R$ ' + str(movimentacao.valor).replace('.', ',') + '0'
            movimentacao.put()
            if movimentacao.tipo == 1:
                total = total + movimentacao.valor
            else:
                total = total - movimentacao.valor
        
        values = {
            'movimentacoesFinanceiras': movimentacoesFinanceiras,
            'total': total,
            'consulta': tipo
        }
        self.response.out.write(template.render('view/financeiro/index.html',values))

    def get(self):
        #self.response.headers['Content-Type'] = 'text/html; charset=iso-8859-1'
        if self.auth():
            if self.acesso('Financeiro'):
                action = self.request.get('action')
                if action == '':
                    action = 'list'
                if action == 'new':
                    self.new()
                if action == 'list':
                    self.list('1')
                if action == 'edit':
                    self.edit(self.request.get('id'))
                if action == 'delete':
                    self.delete()
                if action == 'save':
                    mensagem =self.validar() 
                    if mensagem=='':
                        self.save()
                    else:
                        self.retornaMensagem(mensagem)
                if action == 'warn':
                    self.warn(self.request.get('id'), True)
            else:
                self.list('2')                

    def post(self):
        self.get()
    
    def validar(self):
        if len(self.request.get('data')) > 0:
            try: 
                mydatahora = datetime.strptime(self.request.get('data'), '%d/%m/%Y')
            except:
                return 'Data inv&#225;lida'
        else:
            return 'Data inv&#225;lida'

        if len(self.request.get('valor'))==0 or not (self.request.get('valor').isalnum()):
            return 'Valor inv&#225;lido'
        
        return ''
    
    def retornaMensagem(self, mensagem):
        movimentacaoFinanceira = MovimentacaoFinanceira(peladeiro=None, dataHora=datetime.today(), valor=0.0, tipo=1, motivo=None)
        peladeiros = Peladeiro.gql("ORDER BY nome")
        peladeiroAtual = Peladeiro.get(self.request.get('peladeiro'))
        motivos = MotivoMovFinanceira.all()
        
        values = {
                'peladeiros': peladeiros,
                'motivos': motivos,
                'data': self.request.get('data'),
                'valor':self.request.get('valor'),
                'tipo': self.request.get('tipo'),
                'mensagem':mensagem,
                'peladeiroAtual':peladeiroAtual,
                 'comentario': self.request.get('comentario')
                }
        
        self.response.out.write(template.render('view/financeiro/form.html',values))
    
    def save(self):
        if self.request.get('id') == '':
            movimentacaoFinanceira = MovimentacaoFinanceira()
       
        try:
            mydatahora = datetime.strptime(self.request.get('data'), '%d/%m/%Y')
        except:
            mydatahora = datetime.today()
        movimentacaoFinanceira.data = mydatahora
        movimentacaoFinanceira.peladeiro = Peladeiro.get(self.request.get('peladeiro'))
        movimentacaoFinanceira.valor = float(self.request.get('valor'))
        movimentacaoFinanceira.tipo = int(self.request.get('tipo'))
        movimentacaoFinanceira.motivo = MotivoMovFinanceira.get(self.request.get('motivo'))
        movimentacaoFinanceira.comentario = self.request.get('comentario')
        key = movimentacaoFinanceira.put()
        self.list('1')
        
    def delete(self):
        key = self.request.get('id')
        movimentacaoFinanceira = MovimentacaoFinanceira.get(key)
        db.delete(movimentacaoFinanceira)
        self.list('1')
        
    def edit(self, key):
        movimentacaoFinanceira = MovimentacaoFinanceira.get(key)
        movimentacaoFinanceira.id = key
        values = {
            'movimentacaoFinanceira': movimentacaoFinanceira
        }
        self.response.out.write(template.render('view/financeiro/form.html', values))    
    
    def auth(self):
        if not users.get_current_user():
            if self.request.method != 'GET':
                self.redirect('/?action=logon')
            else:
                url = users.create_login_url(self.request.uri)
                self.redirect(url)
        else:
            peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
            if not peladeiro.confirmacao:
                self.redirect('/')
            else:
                return True
            
    def acesso(self, modulo):
        modulo = Modulo.all().filter('nome = ', modulo).get()
        peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
        peladeiroAcesso = PeladeiroAcesso.all().filter('peladeiro = ', peladeiro).filter('modulo = ', modulo).get()
        if peladeiroAcesso!=None:
            return True
        else:
            #self.response.out.write(template.render('view/semacesso.html',''))
            return False
        