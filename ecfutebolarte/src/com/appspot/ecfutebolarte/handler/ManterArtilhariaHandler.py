from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import login_required  
from google.appengine.api import mail
from datetime import datetime
from com.appspot.ecfutebolarte.db.Pelada import Pelada
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.Presenca import Presenca
from com.appspot.ecfutebolarte.db.Artilharia import Artilharia

class ManterArtilhariaHandler(webapp.RequestHandler):

    def new(self):
        self.response.headers['Content-Type'] = 'text/html; charset=iso-8859-1'
    
        #artilharia = Artilharia(peladeiro=None, pelada=None, presenca=None, gols='')
        peladeiro = Peladeiro(user=None,nome=None,apelido=None,telefone=None,confirmacao=None, gols='0', numeroPartidas=None)
        #peladeiros = Peladeiro.all()
       
        values = {
                'peladeiros': peladeiro                
                }
             
        self.response.out.write(template.render('view/artilharia/form.html',values))
        
        
    def post(self):
        self.process()

    def get(self):
        self.process()
        
        
    def auth(self):
        if not users.get_current_user():
            if self.request.method != 'GET':
                self.redirect('/?action=logon')
            else:
                url = users.create_login_url(self.request.uri)
                self.redirect(url)
        else:
            peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
            if not peladeiro.confirmacao:
                self.redirect('/')
            else:
                return True

    def process(self):
        if self.auth():
            action = self.request.get('action')
            if action == '':
                action = 'list'
            if action == 'new':
                self.new()
            if action == 'list':
                self.list()
            if action == 'edit':
                self.edit(self.request.get('id'))
            if action == 'delete':
                self.delete()
            if action == 'save':
                self.save()
            if action == 'warn':
                self.warn(self.request.get('id'), True)
     

    def save(self):
        peladeiro = Peladeiro.get(self.request.get('id'))        
        peladeiro.gols = self.request.get('gols')
        key = peladeiro.put()
        #message = {
        #        'info':'Gols do peladeiro "' + self.request.get('user') + '" atualizado com sucesso.'
        #}        
        self.edit(key)
        self.redirect('/artilharia')
              
        

    def edit(self, key):
        artilharia = Peladeiro.get(key)
        artilharia.id = key
        values = {
            'artilharia': artilharia
        }
        self.response.out.write(template.render('view/artilharia/form.html', values))    

    def list(self):        
        #self.response.headers['Content-Type'] = 'text/html; charset=iso-8859-1'
        peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
        artilheiros = Peladeiro.gql("ORDER BY nome ASC")
        values = {                  
            'artilheiros': artilheiros,
            'peladeiro': peladeiro
        }
        
        self.response.out.write(template.render('view/artilharia/index.html', values))
        

