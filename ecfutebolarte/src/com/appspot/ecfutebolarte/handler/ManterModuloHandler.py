from google.appengine.api import mail
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import login_required
from datetime import datetime
from com.appspot.ecfutebolarte.db.Peladeiro import Peladeiro
from com.appspot.ecfutebolarte.db.Modulo import Modulo

class ManterModuloHandler(webapp.RequestHandler):

    def post(self):
        self.process()

    def get(self):
        self.process()
        
    def process(self):
        self.response.headers['Content-Type'] = 'text/html; charset=iso-8859-1'
        if self.auth() and self.acesso():
            action = self.request.get('action')
            if action == '':
                self.list()
            if action == 'list':
                self.list()
            if action == 'edit':
                self.edit(self.request.get('id'), {})
            if action == 'new':
                self.new()
            if action == 'save':
                self.save()
            if action == 'delete':
                self.delete()
  
    def list(self):
        modulos = Modulo.all()
        values = {
            'modulos': modulos
        }
        self.response.out.write(template.render('view/modulo/index.html', values))

    def edit(self, key, message):
        modulo = Modulo.get(key)
        modulo.id = key
        values = {
            'message': message,
            'modulo': modulo
        }
        self.response.out.write(template.render('view/modulo/form.html', values))

    def auth(self):
        if not users.get_current_user():
            if self.request.method != 'GET':
                self.redirect('/?action=logon')
            else:
                url = users.create_login_url(self.request.uri)
                self.redirect(url)
        else:
            peladeiro = Peladeiro.all().filter('user = ', users.get_current_user()).get()
            if not peladeiro.confirmacao:
                self.redirect('/')
            else:
                return True

    def save(self):
        if self.request.get('id')=='':
            modulo = Modulo()
            modulo.nome = self.request.get('nome')
            modulo.put()
        else:
            chave=self.request.get('id')
            modulo = Modulo.get(chave)
            modulo.nome = self.request.get('nome')
            modulo.save()
        self.list()            
            
        
    def new(self):
        self.response.headers['Content-Type'] = 'text/html; charset=iso-8859-1'

        modulo = Modulo(nome=None)
       
        values = {
                'modulo': modulo
                }
             
        self.response.out.write(template.render('view/modulo/form.html',values))

    def delete(self):
        modulo = Modulo.get(self.request.get('id'))
        db.delete(modulo)
        self.list()
        
    def acesso(self):
        if users.is_current_user_admin(): 
            return True
        else:
            self.response.out.write(template.render('view/semacesso.html',''))
